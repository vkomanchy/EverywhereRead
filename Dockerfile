FROM node:14.15.3
LABEL maintainer="vkomanchy@gmail.com"
WORKDIR /app

RUN npm i -g npm@8.1.3
RUN apt-get update -y && apt-get install -y
RUN apt-get install ghostscript -y
RUN apt-get install graphicsmagick -y

COPY . .

RUN cd ./front-end && npm install && npm run build
RUN cd ./back-end && npm install

RUN mv -v ./back-end/src . && mv -v ./back-end/node_modules . && mv ./back-end/package.json . && rm -rf back-end && rm -rf front-end

EXPOSE 3000

CMD   ["npm", "run", "start:back"]
