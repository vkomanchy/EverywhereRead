import Vue from "vue"
import VueRouter from "vue-router"

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "Home",
    meta: {
      title: "EverywhereRead | Start your way",
    },

    component: () => import("../views/StartPage.vue"),
  },
  {
    path: "/signup",
    name: "Sign up",
    meta: {
      title: "EverywhereRead | Sign up",
    },

    component: () => import("../views/SignUp.vue"),
  },
  {
    path: "/reader",
    name: "Reader",
    meta: {
      title: "EverywhereRead | Reader",
    },

    component: () => import("../views/ReaderPage.vue"),
  },
  {
    path: "/terms",
    name: "Terms",
    meta: {
      title: "EverywhereRead | Terms",
    },

    component: () => import("../views/TermsPage.vue"),
  }
]

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
