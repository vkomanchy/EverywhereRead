export default {
  inserted: (element) => {
    function loadImage() {
      const imageElement = Array.from(element.children).find((childElement) => childElement.nodeName === "IMG");

      if (imageElement) {
        imageElement.addEventListener("load", () => {
          setTimeout(() => element.classList.add("loaded"), 100)
        });

        imageElement.addEventListener("error", () => {
          console.error("Error while getting image");
        });

        imageElement.src = imageElement.dataset.url;
      }
    }

    function handleIntersect(entries, observer) {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          loadImage();
          observer.unobserve(element);
        }
      });
    }

    function createObserver() {
      const options = {
        root: null,
        threshold: 0,
      };

      const observer = new IntersectionObserver(handleIntersect, options);

      observer.observe(element);
    }

    if (window["IntersectionObserver"]) {
      createObserver();
    } else {
      loadImage();
    }
  }
};
