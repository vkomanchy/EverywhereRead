export default {
  inserted: (element, _bind, vnode) => {
    function getPage() {
      const imageElement = Array.from(element.children).find((childElement) => childElement.nodeName === "IMG");

      if (imageElement) {
        vnode.context.setCurrentPage(imageElement.dataset.pageNumber);

        if (!vnode.context.isChildrenLoaded) {
          vnode.context.setIsChildrenLoaded(true);
        }
      }
    }

    function handleIntersect(entries) {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          getPage();
        }
      });
    }

    function createObserver() {
      const options = {
        root: null,
        threshold: 0,
      };

      const observer = new IntersectionObserver(handleIntersect, options);

      observer.observe(element);
    }

    if (window["IntersectionObserver"]) {
      createObserver();
    } else {
      console.error("Your browser can't use this site");
    }
  }
};
