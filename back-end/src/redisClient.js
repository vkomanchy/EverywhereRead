const redis = require('redis');
const { promisify } = require('util');

const REDIS_HOST = process.env.REDIS_HOST || 'localhost';
const REDIS_PORT = process.env.REDIS_PORT || '6379';
const REDIS_PASSWORD = process.env.REDIS_PASSWORD;

let redisClient = redis.createClient(
  {
    url: `redis://${REDIS_HOST}:${REDIS_PORT}`,
    password: REDIS_PASSWORD,
  },
);

redisClient.on('error', function (error) {
  console.error('Error with redis client', error);
});

const getAsync = promisify(redisClient.get).bind(redisClient);
const existsAsync = promisify(redisClient.exists).bind(redisClient);

const deleteAsync = promisify(redisClient.del).bind(redisClient);

const hgetAsync = promisify(redisClient.hget).bind(redisClient);

const setAsync = promisify(redisClient.set).bind(redisClient);
const hSetAsync = promisify(redisClient.hset).bind(redisClient);

const incrAsync = promisify(redisClient.incr).bind(redisClient);

const rpushAsync = promisify(redisClient.RPUSH).bind(redisClient);
const lrangeAsync = promisify(redisClient.LRANGE).bind(redisClient);

module.exports = {
  redisClient,

  getAsync,
  deleteAsync,
  existsAsync,
  hgetAsync,
  setAsync,
  hSetAsync,
  incrAsync,

  rpushAsync,
  lrangeAsync
};
