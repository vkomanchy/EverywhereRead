const authCheckMiddleware = (req, res, next) => {
  if (req.user) {
    next();
  } else {
    return res.status(401).send('Unathorized');
  }
};

module.exports = authCheckMiddleware;
